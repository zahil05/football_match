package personal.dicoding.submission2.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class DatabaseHelper(ctx: Context):ManagedSQLiteOpenHelper(ctx, "FavoriteMatch.db", null, 1) {
    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DatabaseHelper {
            if (instance == null) {
                instance = DatabaseHelper(ctx.applicationContext)
            }
            return instance as DatabaseHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(FavoriteEvent.TABLE_FAVORITE, true,
                FavoriteEvent.ID_EVENT to INTEGER+ PRIMARY_KEY+ AUTOINCREMENT,
                FavoriteEvent.ID_LEAGUE to TEXT,
                FavoriteEvent.DATE_EVENT to TEXT,
                FavoriteEvent.ID_AWAY_TEAM to TEXT,
                FavoriteEvent.ID_HOME_TEAM to TEXT,
                FavoriteEvent.AWAY_TEAM to TEXT,
                FavoriteEvent.HOME_TEAM to TEXT,
                FavoriteEvent.AWAY_SCORE to INTEGER,
                FavoriteEvent.HOME_SCORE to INTEGER,
                FavoriteEvent.AWAY_GOAL_DETAILS to TEXT,
                FavoriteEvent.HOME_GOAL_DETAILS to TEXT,
                FavoriteEvent.AWAY_SHOTS to INTEGER,
                FavoriteEvent.HOME_SHOTS to INTEGER,
                FavoriteEvent.AWAY_LINEUP_GOALKEEPER to TEXT,
                FavoriteEvent.HOME_LINEUP_GOALKEEPER to TEXT,
                FavoriteEvent.AWAY_LINEUP_DEFENSE to TEXT,
                FavoriteEvent.HOME_LINEUP_DEFENSE to TEXT,
                FavoriteEvent.AWAY_LINEUP_MIDFIELD to TEXT,
                FavoriteEvent.HOME_LINEUP_MIDFIELD to TEXT,
                FavoriteEvent.AWAY_LINEUP_FORWARD to TEXT,
                FavoriteEvent.HOME_LINEUP_FORWARD to TEXT,
                FavoriteEvent.AWAY_LINEUP_SUBSTITUTES to TEXT,
                FavoriteEvent.HOME_LINEUP_SUBSTITUTES to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) =
            db.dropTable(FavoriteEvent.TABLE_FAVORITE, true)
}

val Context.database: DatabaseHelper
    get() = DatabaseHelper.getInstance(applicationContext)