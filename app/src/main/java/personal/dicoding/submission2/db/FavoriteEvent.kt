package personal.dicoding.submission2.db


data class FavoriteEvent(
        var idEvent: Int? = 0,
        var idLeague: String? = "",
        var dateEvent: String? = "",
        var idAwayTeam: String? = "",
        var idHomeTeam: String? = "",
        var awayTeam: String? = "",
        var homeTeam: String? = "",
        var awayScore: Int?,
        var homeScore: Int?,
        var awayGoalDetails: String? = "",
        var strHomeGoalDetails: String? = "",
        var awayShots: Int? = 0,
        var homeShots: Int? = 0,
        var strAwayLineupGoalkeeper: String? = "",
        var homeLineupGoalkeeper: String? = "",
        var awayLineupDefense: String? = "",
        var homeLineupDefense: String? = "",
        var strAwayLineupMidfield: String? = "",
        var homeLineupMidfield: String? = "",
        var awayLineupForward: String? = "",
        var strHomeLineupForward: String? = "",
        var awayLineupSubstitutes: String? = "",
        var homeLineupSubstitutes: String? = "") {

    companion object {
        const val TABLE_FAVORITE:String             = "TABLE_FAVORITE"
        const val ID_EVENT:String                   = "ID_EVENT"
        const val ID_LEAGUE:String                  = "ID_LEAGUE"
        const val DATE_EVENT:String                 = "DATE_EVENT"
        const val HOME_TEAM:String                  = "HOME_TEAM"
        const val AWAY_TEAM:String                  = "AWAY_TEAM"
        const val ID_HOME_TEAM:String               = "ID_HOME_TEAM"
        const val ID_AWAY_TEAM:String               = "ID_AWAY_TEAM"
        const val HOME_SCORE :String                = "HOME_SCORE"
        const val AWAY_SCORE :String                = "AWAY_SCORE"
        const val HOME_GOAL_DETAILS:String          = "HOME_GOAL_DETAILS"
        const val AWAY_GOAL_DETAILS:String          = "AWAY_GOAL_DETAILS"
        const val HOME_SHOTS:String                 = "HOME_SHOTS"
        const val AWAY_SHOTS:String                 = "AWAY_SHOTS"
        const val AWAY_LINEUP_GOALKEEPER:String     = "AWAY_LINEUP_GOALKEEPER"
        const val HOME_LINEUP_GOALKEEPER:String     = "HOME_LINEUP_GOALKEEPER"
        const val AWAY_LINEUP_FORWARD:String        = "AWAY_LINEUP_FORWARD"
        const val HOME_LINEUP_FORWARD:String        = "HOME_LINEUP_FORWARD"
        const val AWAY_LINEUP_DEFENSE:String        = "AWAY_LINEUP_DEFENSE"
        const val HOME_LINEUP_DEFENSE:String        = "HOME_LINEUP_DEFENSE"
        const val AWAY_LINEUP_MIDFIELD:String       = "AWAY_LINEUP_MIDFIELD"
        const val HOME_LINEUP_MIDFIELD:String       = "HOME_LINEUP_MIDFIELD"
        const val AWAY_LINEUP_SUBSTITUTES: String   = "AWAY_LINEUP_SUBSTITUTES"
        const val HOME_LINEUP_SUBSTITUTES: String   = "HOME_LINEUP_SUBSTITUTES"
    }
}
