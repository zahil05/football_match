package personal.dicoding.submission2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main_host.*
import personal.dicoding.submission2.R.id.*
import personal.dicoding.submission2.R.layout.activity_main_host
import personal.dicoding.submission2.home.fragment.favoriteMatch.FavoriteMatchFragment
import personal.dicoding.submission2.home.fragment.lastMatch.LastMatchFragment
import personal.dicoding.submission2.home.fragment.nextMatch.NextMatchFragment

class MainHostActivity : AppCompatActivity() {
    private val lastFragment:String = "last_fragment"
    private val nextFragment:String = "next_fragment"
    private val favoriteFragment:String = "favorite_fragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_main_host)

        loadLastMatchFragment(savedInstanceState)

        navigation.setOnNavigationItemSelectedListener{
            item-> when(item.itemId){
                navigation_lastMatch->{
                    loadLastMatchFragment(savedInstanceState)
                }

                navigation_nextMatch->{
                    loadNextMatchFragment(savedInstanceState)
                }

                favorite_match->{
                    loadFavoritMatchFragment(savedInstanceState)
                }
            }
            true
        }
    }

    private fun loadLastMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .addToBackStack(lastFragment)
                    .replace(R.id.main_container, LastMatchFragment())
                    .commit()
        }
    }

    private fun loadNextMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .addToBackStack(nextFragment)
                    .replace(R.id.main_container, NextMatchFragment())
                    .commit()
        }
    }

    private fun loadFavoritMatchFragment(savedInstanceState:Bundle?){
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .addToBackStack(favoriteFragment)
                    .replace(R.id.main_container, FavoriteMatchFragment())
                    .commit()
        }
    }

}
