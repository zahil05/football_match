package personal.dicoding.submission2.retrofit

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class DisposeableManager {
    private var compositeDisposable:CompositeDisposable? = null

    fun add(disposable: Disposable){
        if(compositeDisposable==null){
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable?.add(disposable)
    }

    fun dispose(){
        compositeDisposable?.dispose()
        compositeDisposable = null
    }
}