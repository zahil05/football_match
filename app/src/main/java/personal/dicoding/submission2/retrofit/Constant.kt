package personal.dicoding.submission2.retrofit

class Constant {
    companion object {
        const val CONNECT_TIME_OUT : Long = 60
        const val WRITE_TIME_OUT :Long= 60
        const val READ_TIME_OUT : Long = 60
    }
}