package personal.dicoding.submission2.retrofit

import io.reactivex.Observable
import personal.dicoding.submission2.home.model.ResponseDetailEvent
import personal.dicoding.submission2.home.model.ResponseEvent
import personal.dicoding.submission2.home.model.ResponseTeam
import retrofit2.http.GET
import retrofit2.http.Query

interface API {
        @GET("eventsnextleague.php")
        fun getNextLeague(@Query("id") id: String):
                Observable<ResponseEvent>

        @GET("eventspastleague.php")
        fun getLastLeague(@Query("id") id: String):
            Observable<ResponseEvent>

        @GET("lookupevent.php")
        fun getDetailEvent(@Query("id") id: String):
            Observable<ResponseDetailEvent>

        @GET("lookupteam.php")
        fun getTeamBadge(@Query("id") id: String):
            Observable<ResponseTeam>
}