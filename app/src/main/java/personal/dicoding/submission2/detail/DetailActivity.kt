package personal.dicoding.submission2.detail

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log.e
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import org.jetbrains.anko.*
import org.jetbrains.anko.db.*
import org.jetbrains.anko.design.snackbar
import personal.dicoding.submission2.*
import personal.dicoding.submission2.R.drawable.ic_add_to_favorites
import personal.dicoding.submission2.R.drawable.ic_added_to_favorites
import personal.dicoding.submission2.db.FavoriteEvent
import personal.dicoding.submission2.db.database
import personal.dicoding.submission2.home.model.DetailEvent
import personal.dicoding.submission2.home.model.ResponseDetailEvent
import personal.dicoding.submission2.retrofit.API
import personal.dicoding.submission2.retrofit.Service

class DetailActivity: AppCompatActivity(), AnkoComponent<Context>, DetailView {
    companion object {

        const val KEY :String = "id"
        const val KEY_FROM : String = "from"
    }
    private var idEvent : Int = 0

    private var from : Int = 0
    private var isFavorite:Boolean = false
    private var menuItem: Menu? = null

    private lateinit var tvDate : TextView

    private lateinit var tvHomeScore: TextView
    private lateinit var tvAwayScore: TextView
    private lateinit var tvHomeTeam : TextView
    private lateinit var tvAwayTeam: TextView
    private lateinit var ivHomeTeam : ImageView
    private lateinit var ivAwayTeam : ImageView
    private lateinit var progressBar: ProgressBar
    private lateinit var presenter: DetailPresenter
    private lateinit var tvHomeGoal:TextView
    private lateinit var tvAwayGoal:TextView
    private lateinit var tvAwayShots:TextView

    private lateinit var tvHomeShots:TextView
    private lateinit var tvAwayGoalkeeper:TextView

    private lateinit var tvHomeGoalkeeper:TextView
    private lateinit var tvAwayDefense:TextView

    private lateinit var tvHomeDefense:TextView
    private lateinit var tvAwayMidfielder:TextView

    private lateinit var tvHomeMidfielder:TextView
    private lateinit var tvAwayForward:TextView

    private lateinit var tvHomeForward:TextView
    private lateinit var tvAwaySubtitutes:TextView

    private lateinit var tvHomeSubtitutes:TextView
    private lateinit var detailEvent : DetailEvent

    private lateinit var layout:LinearLayout
    private lateinit var api:API

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(createView(AnkoContext.create(this)))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val intent = intent
        idEvent = intent.getIntExtra(KEY, 0)
        from = intent.getIntExtra(KEY_FROM, 0)

        favoriteState()
        api = Service().create()
        presenter = DetailPresenter(ctx, this, api)
        presenter.getDetailEvent(idEvent, from )
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.actionbar_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem)= when (item.itemId){
        R.id.action_favorite->{
            if (isFavorite) deleteFromFavorite(idEvent.toString()) else addToFavorite()
            isFavorite = !isFavorite
            setFavorite()
            true
        }
        else->{super.onOptionsItemSelected(item)}
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private fun favoriteState(){
        database.use {
            val result = select(FavoriteEvent.TABLE_FAVORITE)
                    .whereArgs("(ID_EVENT = {ID_EVENT})",
                            "ID_EVENT" to idEvent)
            val favorite = result.parseList(classParser<FavoriteEvent>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    private fun addToFavorite() {
        try{
            database.use{
                insert(FavoriteEvent.TABLE_FAVORITE ,
                FavoriteEvent.ID_EVENT to detailEvent.idEvent ,
                        FavoriteEvent.DATE_EVENT to detailEvent.dateEvent,
                        FavoriteEvent.ID_HOME_TEAM to detailEvent.idHomeTeam ,
                        FavoriteEvent.ID_AWAY_TEAM to detailEvent.idAwayTeam ,
                        FavoriteEvent.AWAY_TEAM to detailEvent.strAwayTeam,
                        FavoriteEvent.HOME_TEAM to detailEvent.strHomeTeam,
                        FavoriteEvent.AWAY_SCORE to detailEvent.intAwayScore,
                        FavoriteEvent.HOME_SCORE to detailEvent.intHomeScore,
                        FavoriteEvent.AWAY_SHOTS to detailEvent.intAwayShots,
                        FavoriteEvent.HOME_SHOTS to detailEvent.intHomeShots,
                        FavoriteEvent.AWAY_GOAL_DETAILS to detailEvent.strAwayGoalDetails,
                        FavoriteEvent.HOME_GOAL_DETAILS to detailEvent.strHomeGoalDetails,
                        FavoriteEvent.AWAY_LINEUP_GOALKEEPER to detailEvent.strAwayLineupGoalkeeper,
                        FavoriteEvent.HOME_LINEUP_GOALKEEPER to detailEvent.strHomeLineupGoalkeeper,
                        FavoriteEvent.AWAY_LINEUP_DEFENSE to detailEvent.strAwayLineupDefense,
                        FavoriteEvent.HOME_LINEUP_DEFENSE to detailEvent.strHomeLineupDefense,
                        FavoriteEvent.AWAY_LINEUP_MIDFIELD to detailEvent.strAwayLineupMidfield,
                        FavoriteEvent.HOME_LINEUP_MIDFIELD to detailEvent.strHomeLineupMidfield,
                        FavoriteEvent.AWAY_LINEUP_FORWARD to detailEvent.strAwayLineupForward,
                        FavoriteEvent.HOME_LINEUP_FORWARD to detailEvent.strHomeLineupForward,
                        FavoriteEvent.AWAY_LINEUP_SUBSTITUTES to detailEvent.strAwayLineupSubstitutes,
                        FavoriteEvent.HOME_LINEUP_SUBSTITUTES to detailEvent.strHomeLineupSubstitutes
                )
            }
            snackbar(layout, R.string.added_to_favorite).show()
        }catch (e:SQLiteConstraintException){
            snackbar(layout, e.localizedMessage).show()
        }
    }

    private fun deleteFromFavorite(id:String) = try {
        database.use {
            delete(FavoriteEvent.TABLE_FAVORITE, "ID_EVENT={ID_EVENT}",
                    "ID_EVENT" to id)

        }
        snackbar(layout, R.string.remove_from_favirite).show()
    } catch (e: SQLiteConstraintException){
        snackbar(layout, e.localizedMessage).show()
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }

    override fun onStop() {
        super.onStop()
        presenter.disposeAllObservable()
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun loadData(data: ResponseDetailEvent) {
        detailEvent = data.events[0]
        tvDate.text         = detailEvent.dateEvent.toLocalDate()
        tvHomeTeam.text     = detailEvent.strHomeTeam
        tvAwayTeam.text     = detailEvent.strAwayTeam
        tvAwayScore.text    = detailEvent.intAwayScore.printIfNotNull()
        tvHomeScore.text    = detailEvent.intHomeScore.printIfNotNull()
        tvAwayGoal.text    = detailEvent.strAwayGoalDetails.printIfNotNull()
        tvHomeGoal.text    = detailEvent.strHomeGoalDetails.printIfNotNull()

        tvAwayShots.text     = detailEvent.intAwayShots.printIfNotNull()
        tvHomeShots.text     = detailEvent.intHomeShots.printIfNotNull()

        tvAwayGoalkeeper.text= detailEvent.strAwayLineupGoalkeeper.printIfNotNull()
        tvHomeGoalkeeper.text= detailEvent.strHomeLineupGoalkeeper.printIfNotNull()

        tvAwayDefense.text = detailEvent.strAwayLineupDefense.printIfNotNull()
        tvHomeDefense.text = detailEvent.strHomeLineupDefense.printIfNotNull()

        tvAwayMidfielder.text = detailEvent.strAwayLineupMidfield.printIfNotNull()
        tvHomeMidfielder.text = detailEvent.strHomeLineupMidfield.printIfNotNull()

        tvAwayForward.text    = detailEvent.strAwayLineupForward.printIfNotNull()
        tvHomeForward.text    = detailEvent.strHomeLineupForward.printIfNotNull()

        tvAwaySubtitutes.text = detailEvent.strAwayLineupSubstitutes.printIfNotNull()
        tvHomeSubtitutes.text = detailEvent.strHomeLineupSubstitutes.printIfNotNull()

        presenter.getAwayTeamImage(detailEvent.idAwayTeam.toString())
        presenter.getHomeTeamImage(detailEvent.idHomeTeam.toString())
    }

    override fun loadDataFromDB(data: FavoriteEvent) {
        e("loadDataFromDB", data.toString())
        tvDate.text         = data.dateEvent.toLocalDate()
        tvHomeTeam.text     = data.homeTeam
        tvAwayTeam.text     = data.awayTeam
        tvAwayScore.text    = data.awayScore.printIfNotNull()
        tvHomeScore.text    = data.homeScore.printIfNotNull()
        tvAwayGoal.text     = data.awayGoalDetails.printIfNotNull()
        tvHomeGoal.text     = data.strHomeGoalDetails.printIfNotNull()

        tvAwayShots.text     = data.awayShots.printIfNotNull()
        tvHomeShots.text     = data.homeShots.printIfNotNull()

        tvAwayGoalkeeper.text= data.strAwayLineupGoalkeeper.printIfNotNull()
        tvHomeGoalkeeper.text= data.homeLineupGoalkeeper.printIfNotNull()

        tvAwayDefense.text = data.awayLineupDefense.printIfNotNull()
        tvHomeDefense.text = data.homeLineupDefense.printIfNotNull()

        tvAwayMidfielder.text = data.strAwayLineupMidfield.printIfNotNull()
        tvHomeMidfielder.text = data.homeLineupMidfield.printIfNotNull()

        tvAwayForward.text    = data.awayLineupForward.printIfNotNull()
        tvHomeForward.text    = data.strHomeLineupForward.printIfNotNull()

        tvAwaySubtitutes.text = data.awayLineupSubstitutes.printIfNotNull()
        tvHomeSubtitutes.text = data.homeLineupSubstitutes.printIfNotNull()

        presenter.getAwayTeamImage(data.idAwayTeam.toString())
        presenter.getHomeTeamImage(data.idHomeTeam.toString())

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.disposeAllObservable()
    }

    override fun loadImageAwayTeam(imageUrl: String?) {
        try{Glide.with(ctx).load(imageUrl).into(ivAwayTeam)}
        catch (e:Exception){e.printStackTrace()}
    }

    override fun loadImageHomeTeam(imageUrl: String?) {
        try{Glide.with(ctx).load(imageUrl).into(ivHomeTeam)}
        catch (e:Exception){e.printStackTrace()}
    }

    override fun createView(ui: AnkoContext<Context>): View  = with(ui) {

        scrollView {
            lparams(width = matchParent, height = matchParent)
            layout = verticalLayout {
                padding = dip(10)
                lparams(width = matchParent, height = wrapContent)

                progressBar = progressBar {  }.lparams{gravity = Gravity.CENTER}

                tvDate = textView {
                    typeface = Typeface.DEFAULT_BOLD
                    textSize = 16f
                    textColor = Color.BLUE
                    gravity = Gravity.CENTER_HORIZONTAL
                }.lparams(width = matchParent, height = wrapContent) {
                    bottomMargin = dip(5)
                }

                linearLayout {
                    rightPadding = dip(16)
                    lparams(width = matchParent, height = wrapContent)

                    //homeTeam
                    verticalLayout {
                        ivHomeTeam = imageView {
                        }.lparams(width = dip(60), height = dip(60)) {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }

                        tvHomeTeam = textView {
                            ellipsize = TextUtils.TruncateAt.END
                            singleLine = true
                            gravity = Gravity.CENTER_HORIZONTAL
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(width = matchParent, height = wrapContent){
                            topMargin = dip( 3)
                        }
                    }.lparams(width = 0, height = wrapContent, weight = 0.25f)

                    //score
                    verticalLayout {
                        tvHomeScore = textView {
                            gravity = Gravity.CENTER
                            typeface = Typeface.DEFAULT_BOLD
                            textSize = 20f
                        }.lparams(width = matchParent, height = matchParent)

                    }.lparams(width = 0, height = matchParent, weight = 0.2f)

                    verticalLayout {

                        textView(R.string.vs) {
                            gravity = Gravity.CENTER
                            textSize = 14f
                        }.lparams(width = matchParent, height = matchParent)

                    }.lparams(width = 0, height = matchParent, weight = 0.1f)

                    verticalLayout {

                        tvAwayScore = textView {
                            gravity = Gravity.CENTER
                            typeface = Typeface.DEFAULT_BOLD
                            textSize = 20f
                        }.lparams(width = matchParent, height = matchParent)

                    }.lparams(width = 0, height = matchParent, weight = 0.2f)

                    verticalLayout {

                        ivAwayTeam = imageView {
                        }.lparams(width = dip(60), height = dip(60))
                        { gravity = Gravity.CENTER }

                        tvAwayTeam = textView {
                            gravity = Gravity.CENTER
                            singleLine = true
                            typeface = Typeface.DEFAULT_BOLD
                            ellipsize = TextUtils.TruncateAt.END
                        }.lparams(width = matchParent, height = wrapContent){
                            topMargin = dip( 3)
                        }

                    }.lparams(width = 0, height = wrapContent
                            , weight = 0.25f){
                        gravity = Gravity.CENTER
                    }
                }

                verticalLayout {
                    padding = dip(16)
                    lparams(width = matchParent, height = wrapContent)

                    view {
                        backgroundColor = Color.BLUE
                    }.lparams(width = matchParent, height = dip(1))
                    { topMargin = dip(5)
                        bottomMargin = dip(5) }

                    // goals detail
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)

                        tvHomeGoal = textView {
                        gravity = Gravity.START

                        }.lparams(width = 0, height = wrapContent
                                , weight = 0.35f)
                        textView(R.string.goal) {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams(width = 0, height = wrapContent
                                , weight = 0.3f)

                        tvAwayGoal = textView {
                            gravity = Gravity.END
                        }.lparams(width = 0, height = wrapContent
                                , weight = 0.35f)
                    }

                    view {
                        backgroundColor = Color.BLUE
                    }.lparams(width = matchParent, height = dip(1)){
                        bottomMargin=dip(5)
                        topMargin = dip(5)}

                    // shots detail
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)

                        tvHomeShots = textView {
                            gravity = Gravity.START}.lparams(width = 0, height = wrapContent
                                , weight = 0.35f)
                        textView(R.string.shots) {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams(width = 0, height = wrapContent
                                , weight = 0.3f)
                        tvAwayShots = textView {
                            gravity = Gravity.END
                        }.lparams(width = 0, height = wrapContent
                                , weight = 0.35f)
                    }

                    view {
                        backgroundColor = Color.BLUE
                    }.lparams(width = matchParent, height = dip(1)){
                        bottomMargin = dip(5)
                        topMargin=dip(5)
                    }
                }

                // lineups
                verticalLayout {
                    padding = dip(16)
                    lparams(width = matchParent, height = wrapContent)

                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)

                        textView(R.string.lineups) {
                            gravity = Gravity.CENTER_HORIZONTAL
                            textSize = 16F
                            textColor = Color.BLUE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(width = matchParent, height = wrapContent)
                        {bottomMargin = dip(10)}
                    }
                    view {
                        backgroundColor = Color.BLUE
                    }.lparams(width = matchParent, height = dip(1))

                    // lineup goal keeper
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)

                        tvHomeGoalkeeper = textView {
                            gravity = Gravity.START}.lparams(width = 0, height = wrapContent, weight = 0.35f) {
                            margin = dip(5)
                        }

                        textView(R.string.goalkeeper) {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams(width = 0, height = wrapContent, weight = 0.3f) {
                            margin = dip(5)
                        }

                        tvAwayGoalkeeper = textView {
                            gravity = Gravity.END
                        }.lparams(width = 0, height = wrapContent, weight = 0.35f) {
                            margin = dip(5)
                        }
                    }

                    view {
                        backgroundColor = Color.BLUE
                    }.lparams(width = matchParent, height = dip(1))

                    // lineup defense
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)

                        tvHomeDefense = textView {
                            gravity = Gravity.START }.lparams(width = 0, height = wrapContent, weight = 0.35f) {
                            margin = dip(5)
                        }

                        textView(R.string.defender) {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams(width = 0, height = wrapContent, weight = 0.3f) {
                            margin = dip(5)
                        }

                        tvAwayDefense = textView {
                            gravity = Gravity.END
                        }.lparams(width = 0, height = wrapContent, weight = 0.35f) {
                            margin = dip(5)
                        }
                    }
                    view {
                        backgroundColor = Color.BLUE
                    }.lparams(width = matchParent, height = dip(1))

                    // lineup midfier
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)

                        tvHomeMidfielder = textView {
                            gravity = Gravity.START }.lparams(width = 0, height = wrapContent
                                , weight = 0.35f) {
                            margin = dip(5)
                        }

                        textView(R.string.midfielders) {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams(width = 0, height = wrapContent
                                , weight = 0.3f) {
                            margin = dip(5)
                        }

                        tvAwayMidfielder = textView {
                            gravity = Gravity.END
                        }.lparams(width = 0, height = wrapContent
                                ,weight = 0.35f) {
                            margin = dip(5)
                        }
                    }
                    view {
                        backgroundColor = Color.BLUE
                    }.lparams(width = matchParent, height = dip(1))

                    // lineup forward
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)

                        tvHomeForward = textView {
                            gravity = Gravity.START }.lparams(width = 0, height = wrapContent
                                ,weight = 0.35f) {
                            margin = dip(5)
                        }

                        textView(R.string.forward) {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams(width = 0, height = wrapContent
                                ,weight = 0.3f) {
                            margin = dip(5)
                        }

                        tvAwayForward = textView {
                            gravity = Gravity.END
                        }.lparams(width = 0, height = wrapContent
                                ,weight = 0.35f) {
                            margin = dip(5)
                        }
                    }
                    view {
                        backgroundColor = Color.BLUE
                    }.lparams(width = matchParent, height = dip(1))

                    // subtitues
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)

                        tvHomeSubtitutes = textView {
                            gravity = Gravity.START
                        }.lparams(width = 0, height = wrapContent
                                ,weight = 0.35f) {
                            margin = dip(5)
                        }

                        textView(R.string.substitute) {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams(width = 0, height = wrapContent
                                ,weight = 0.3f) {
                            margin = dip(5)
                        }

                        tvAwaySubtitutes = textView {
                            gravity = Gravity.END
                        }.lparams(width = 0, height = wrapContent
                                ,weight = 0.35f) {
                            margin = dip(5)
                        }
                    }
                }
            }
        }
    }
}
