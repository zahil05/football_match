package personal.dicoding.submission2.detail

import android.content.Context
import android.util.Log.e
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.db.SelectQueryBuilder
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.doAsync
import personal.dicoding.submission2.addTo
import personal.dicoding.submission2.db.FavoriteEvent
import personal.dicoding.submission2.db.database
import personal.dicoding.submission2.retrofit.API
import personal.dicoding.submission2.retrofit.DisposeableManager

class DetailPresenter (private val ctx:Context, private val view : DetailView,
                       private val api: API){
    companion object {
        const val FROM_API = 0
        const val FROM_DB = 1
    }

    private val disposeableManager = DisposeableManager()

    fun getDetailEvent(idEvent:Int, from:Int) {
        view.showLoading()
        when(from){
            FROM_API->
                getFromApi(idEvent)
            FROM_DB->
                getFromDB(idEvent)
        }

    }

    private  fun getFromApi(idEvent: Int){
       doAsync {
            api.getDetailEvent(idEvent.toString()).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ result ->
                        view.hideLoading()
                        view.loadData(result)
                    }, {error->
                        view.hideLoading()
                        e("Error", error.message) }
                    )
                    .addTo(disposeableManager)
        }
    }

    private fun getFromDB(idEvent: Int) {
        view.showLoading()
        Observable.just(idEvent).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it->ctx.database.use{
                    val result:SelectQueryBuilder = select(FavoriteEvent.TABLE_FAVORITE)
                            .whereArgs("(ID_EVENT = {ID_EVENT})",
                                    "ID_EVENT" to it)
                    val favorite = result.parseList(classParser<FavoriteEvent>())
                    view.loadDataFromDB(favorite[0])
                    view.hideLoading()
                    }
                }, {error->
                    view.hideLoading()
                    e("Error", error.message)
                })
        .addTo(disposeableManager)
    }

    fun getAwayTeamImage(idTeam:String){
        doAsync {
            api.getTeamBadge(idTeam).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe( { result->view.loadImageAwayTeam(result.teams[0].strTeamBadge)
                    }, {error->e("Error", error.message)})
                    .addTo(disposeableManager)

        }
    }

    fun getHomeTeamImage(idTeam: String){
        doAsync {
            api.getTeamBadge(idTeam).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                        result->view.loadImageHomeTeam(result.teams[0].strTeamBadge)
                    }, {
                        error->
                        e("Error", error.message)}
                    ).addTo(disposeableManager)
        }
    }

    fun disposeAllObservable(){
        disposeableManager.dispose()
    }
}