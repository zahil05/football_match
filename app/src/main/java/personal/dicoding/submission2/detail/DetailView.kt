package personal.dicoding.submission2.detail

import personal.dicoding.submission2.db.FavoriteEvent
import personal.dicoding.submission2.home.model.ResponseDetailEvent

interface DetailView {
    fun showLoading()
    fun hideLoading()
    fun loadData(data : ResponseDetailEvent)
    fun loadDataFromDB(data:FavoriteEvent)
    fun loadImageAwayTeam(imageUrl : String?)
    fun loadImageHomeTeam(imageUrl : String?)
}