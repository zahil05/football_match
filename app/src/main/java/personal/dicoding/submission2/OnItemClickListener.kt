package personal.dicoding.submission2

import android.view.View

interface OnItemClickListener{
    fun onItemClick(v: View, position: Int)
}