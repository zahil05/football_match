package personal.dicoding.submission2

import android.annotation.SuppressLint
import android.view.View
import io.reactivex.disposables.Disposable
import personal.dicoding.submission2.retrofit.DisposeableManager
import java.text.SimpleDateFormat
import java.util.*

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

@SuppressLint("SimpleDateFormat")
fun String?.toLocalDate():String{
    val defaultDateFormat = SimpleDateFormat("yyyy-MM-dd")
    val locale = Locale("id")
    val sdf = SimpleDateFormat("EEE, dd MMM yyyy", locale)
    val date = defaultDateFormat.parse(this)
    return sdf.format(date)
}

fun Int?.printIfNotNull():String =
        this?.toString()?:""

fun String?.printIfNotNull():String
        = this ?:""

fun Disposable.addTo(disposeableManager: DisposeableManager) : Disposable
        =  apply { disposeableManager.add(this) }