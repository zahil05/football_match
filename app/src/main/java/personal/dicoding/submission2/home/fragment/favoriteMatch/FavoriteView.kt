package personal.dicoding.submission2.home.fragment.favoriteMatch

import personal.dicoding.submission2.db.FavoriteEvent

interface FavoriteView {
        fun showLoading()
        fun hideLoading()
        fun showFavoriteList(data : List<FavoriteEvent>)
}