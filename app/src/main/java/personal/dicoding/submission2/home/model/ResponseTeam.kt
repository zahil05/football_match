package personal.dicoding.submission2.home.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ResponseTeam  (val teams: List<DetailTeam>) : Parcelable