package personal.dicoding.submission2.home.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Event(@SerializedName("intHomeShots")
                 var homeShots: Int? = 0,
                 @SerializedName("strSport")
                 var sport: String? = "",
                 @SerializedName("strHomeLineupDefense")
                 var homeLineupDefense: String? = "",
                 @SerializedName("strAwayLineupSubstitutes")
                 var awayLineupSubstitutes: String? = "",
                 @SerializedName("idLeague")
                 var idLeague: String? = "",
                 @SerializedName("idSoccerXML")
                 var idSoccerXML: Int? = 0,
                 @SerializedName("strHomeLineupForward")
                 var strHomeLineupForward: String? = "",
                 @SerializedName("strTVStation")
                 var strTVStation: String? = "",
                 @SerializedName("strHomeGoalDetails")
                 var strHomeGoalDetails: String? = "",
                 @SerializedName("strAwayLineupGoalkeeper")
                 var strAwayLineupGoalkeeper: String? = "",
                 @SerializedName("strAwayLineupMidfield")
                 var strAwayLineupMidfield: String? = "",
                 @SerializedName("idEvent")
                 var idEvent: Int? = 0,
                 @SerializedName("intRound")
                 var round: Int? = 0,
                 @SerializedName("strHomeYellowCards")
                 var homeYellowCards: String? = "",
                 @SerializedName("idHomeTeam")
                 var idHomeTeam: Int? = 0,
                 @SerializedName("intHomeScore")
                 var homeScore: Int? ,
                 @SerializedName("dateEvent")
                 var dateEvent: String? = "",
                 @SerializedName("strCountry")
                 var country: String? = "",
                 @SerializedName("strAwayTeam")
                 var awayTeam: String? = "",
                 @SerializedName("strHomeLineupMidfield")
                 var homeLineupMidfield: String? = "",
                 @SerializedName("strDate")
                 var date: String? = "",
                 @SerializedName("strHomeFormation")
                 var homeFormation: String? = "",
                 @SerializedName("strMap")
                 var map: String? = "",
                 @SerializedName("idAwayTeam")
                 var idAwayTeam: Int? = 0,
                 @SerializedName("strAwayRedCards")
                 var awayRedCards: String? = "",
                 @SerializedName("strBanner")
                 var banner: String? = "",
                 @SerializedName("strFanart")
                 var fanart: String? = "",
                 @SerializedName("strDescriptionEN")
                 var DescriptionEN:String? = "",
                 @SerializedName("strResult")
                 var result: String? = "",
                 @SerializedName("strCircuit")
                 var circuit: String? = "",
                 @SerializedName("intAwayShots")
                 var awayShots: Int? = 0,
                 @SerializedName("strFilename")
                 var filename: String? = "",
                 @SerializedName("strTime")
                 var time: String? = "",
                 @SerializedName("strAwayGoalDetails")
                 var awayGoalDetails: String? = "",
                 @SerializedName("strAwayLineupForward")
                 var awayLineupForward: String? = "",
                 @SerializedName("strLocked")
                 var locked: String? = "",
                 @SerializedName("strSeason")
                 var season: String? = "",
                 @SerializedName("intSpectators")
                 var spectators: Int? = 0,
                 @SerializedName("strHomeRedCards")
                 var homeRedCards: String? = "",
                 @SerializedName("strHomeLineupGoalkeeper")
                 var homeLineupGoalkeeper: String? = "",
                 @SerializedName("strHomeLineupSubstitutes")
                 var homeLineupSubstitutes: String? = "",
                 @SerializedName("strAwayFormation")
                 var awayFormation: String? = "",
                 @SerializedName("strEvent")
                 var event: String? = "",
                 @SerializedName("strAwayYellowCards")
                 var awayYellowCards: String? = "",
                 @SerializedName("strAwayLineupDefense")
                 var awayLineupDefense: String? = "",
                 @SerializedName("strHomeTeam")
                 var homeTeam: String? = "",
                 @SerializedName("strThumb")
                 var thumb: String? = "",
                 @SerializedName("strLeague")
                 var league: String? = "",
                 @SerializedName("intAwayScore")
                 var awayScore: Int?,
                 @SerializedName("strCity")
                 var city: String? = "",
                 @SerializedName("strPoster")
                 var poster: String? = "") : Parcelable