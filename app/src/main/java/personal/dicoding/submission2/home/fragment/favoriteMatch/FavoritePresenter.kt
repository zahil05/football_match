package personal.dicoding.submission2.home.fragment.favoriteMatch

import android.content.Context
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import personal.dicoding.submission2.addTo
import personal.dicoding.submission2.db.FavoriteEvent
import personal.dicoding.submission2.db.database
import personal.dicoding.submission2.retrofit.DisposeableManager

class FavoritePresenter(private val context: Context?, private val view:FavoriteView) {
    private var disposable: Disposable? = null


    fun loadData(){
        view.showLoading()
        disposable = Observable.just(0).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { it ->
                    context?.database?.use {
                        val result = select(FavoriteEvent.TABLE_FAVORITE)
                        val favorites = result.parseList(classParser<FavoriteEvent>())
                        view.showFavoriteList(favorites)
                        view.hideLoading()

                    }
                }
        disposable?.addTo(DisposeableManager())
        }

    fun disposeAllObservable(){
        DisposeableManager().dispose()
        }
    }