package personal.dicoding.submission2.home.fragment.nextMatch

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import personal.dicoding.submission2.OnItemClickListener
import personal.dicoding.submission2.R
import personal.dicoding.submission2.home.event.EventAdapter
import personal.dicoding.submission2.home.event.EventPresenter
import personal.dicoding.submission2.home.event.EventView
import personal.dicoding.submission2.home.model.Event
import personal.dicoding.submission2.home.model.ResponseEvent
import personal.dicoding.submission2.detail.DetailActivity
import personal.dicoding.submission2.detail.DetailPresenter
import personal.dicoding.submission2.invisible
import personal.dicoding.submission2.retrofit.Service
import personal.dicoding.submission2.visible

class NextMatchFragment: Fragment(), AnkoComponent<Context>, EventView , OnItemClickListener {

    private var events: MutableList<Event> = mutableListOf()
    private lateinit var adapter: EventAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var presenter : EventPresenter
    private lateinit var progressBar: ProgressBar

    companion object {
        const val swipeLayout: Int = 1
        const val rvLastMatch: Int = 2
        const val pBar : Int = 10
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showEventList(data: ResponseEvent) {
        swipeRefresh.isRefreshing = false
        events.clear()
        events.addAll(data.events)
        adapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun onStop() {
        super.onStop()
        presenter.disposeAllObservable()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = EventAdapter(events, this)
        listEvent.adapter = adapter

        presenter = EventPresenter(this, Service().create())
        presenter.getNextEventList(getString(R.string.id_league))

        swipeRefresh.setOnRefreshListener {
            events.clear()
            adapter.notifyDataSetChanged()
            presenter.getNextEventList(getString(R.string.id_league))
        }
    }

    override fun onItemClick(v: View, position: Int) {
        val event= events[position]
        startActivity<DetailActivity>(DetailActivity.KEY to event.idEvent,
                DetailActivity.KEY_FROM to DetailPresenter.FROM_API)
    }
    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams (width = matchParent, height = wrapContent)
            backgroundColor = Color.GRAY

            swipeRefresh =swipeRefreshLayout {
                id = swipeLayout
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)
                relativeLayout{
                    lparams(width = matchParent, height = wrapContent)

                    listEvent = recyclerView {
                        id = rvLastMatch
                        lparams (width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)

                    }
                    progressBar = progressBar {
                        id = pBar
                    }.lparams{
                        centerHorizontally()
                    }
                }
            }

        }
    }
}
