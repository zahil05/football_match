package personal.dicoding.submission2.home.event

import personal.dicoding.submission2.home.model.ResponseEvent

interface EventView {
    fun showLoading()
    fun hideLoading()
    fun showEventList(data : ResponseEvent)
}