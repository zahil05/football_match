package personal.dicoding.submission2.home.fragment.favoriteMatch

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import personal.dicoding.submission2.OnItemClickListener
import personal.dicoding.submission2.R
import personal.dicoding.submission2.db.FavoriteEvent
import personal.dicoding.submission2.detail.DetailActivity
import personal.dicoding.submission2.detail.DetailPresenter
import personal.dicoding.submission2.invisible
import personal.dicoding.submission2.visible

class FavoriteMatchFragment: Fragment(), AnkoComponent<Context>, FavoriteView, OnItemClickListener {
    private var favorites: MutableList<FavoriteEvent> = mutableListOf()
    private lateinit var adapter: FavoriteMatchAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var presenter : FavoritePresenter
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh:SwipeRefreshLayout

    companion object {
        const val swipeLayout: Int = 20
        const val rvLastMatch: Int = 21
        const val pBar : Int = 22
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = FavoriteMatchAdapter(favorites, this)

        listEvent.adapter = adapter

        swipeRefresh.setOnRefreshListener {
            favorites.clear()
            adapter.notifyDataSetChanged()
            presenter.loadData()
        }

        presenter = FavoritePresenter(context,this)
        presenter.loadData()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }


    override fun onStop() {
        super.onStop()
        presenter.disposeAllObservable()
    }
    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
        }

    override fun showFavoriteList(data: List<FavoriteEvent>) {
        swipeRefresh.isRefreshing = false
        if(data.isEmpty()){
            snackbar(swipeRefresh, R.string.no_favorite)
        }else{
            favorites.clear()
            favorites.addAll(data)
            adapter.notifyDataSetChanged()

        }
    }

    override fun onItemClick(v: View, position: Int) {
        val favorite= favorites[position]
        startActivity<DetailActivity>(DetailActivity.KEY to favorite.idEvent,
                DetailActivity.KEY_FROM to DetailPresenter.FROM_DB)
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams (width = matchParent, height = wrapContent)
            backgroundColor = Color.GRAY

            swipeRefresh =swipeRefreshLayout {
                id = swipeLayout
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)
                relativeLayout{
                    lparams(width = matchParent, height = wrapContent)

                    listEvent = recyclerView {
                        id = rvLastMatch
                        lparams (width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)

                    }
                    progressBar = progressBar {
                        id = pBar
                    }.lparams{
                        centerHorizontally()
                    }
                }
            }

        }
    }
}


