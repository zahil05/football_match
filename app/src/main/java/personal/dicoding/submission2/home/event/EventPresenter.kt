package personal.dicoding.submission2.home.event

import android.util.Log.e
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import personal.dicoding.submission2.addTo
import personal.dicoding.submission2.retrofit.API
import personal.dicoding.submission2.retrofit.DisposeableManager

class EventPresenter (private val view: EventView,
                      private val apiRepository: API){

    private val disposeableManager = DisposeableManager()
    fun getLastEventList(idLeague:String){
        view.showLoading()

        doAsync {
            apiRepository.getLastLeague(idLeague).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result-> view.showEventList(result)
                                view.hideLoading()},
                            { error-> view.hideLoading()
                                e("Error", error.message)}
                    )
                    .addTo(disposeableManager)
        }
    }

    fun getNextEventList(idLeague:String){
        view.showLoading()
        doAsync {
            apiRepository.getNextLeague(idLeague).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result->
                                view.hideLoading()
                                view.showEventList(result)},
                            { error->
                                view.hideLoading()
                                e("Error", error.message)}
                    )
                    .addTo(disposeableManager)
        }
    }

    fun disposeAllObservable(){
        disposeableManager.dispose()
    }
}