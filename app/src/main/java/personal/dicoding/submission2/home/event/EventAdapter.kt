package personal.dicoding.submission2.home.event

import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import personal.dicoding.submission2.home.model.Event
import personal.dicoding.submission2.OnItemClickListener
import personal.dicoding.submission2.toLocalDate
import kotlin.properties.Delegates

class EventAdapter(private val events:List<Event> , private val listener:(OnItemClickListener)) : RecyclerView.Adapter<EventViewHolder>() {

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bindItem(events[position])
    }

    override fun getItemCount(): Int = events.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): EventViewHolder {
        val view = EventViewHolder(EventUI().createView(AnkoContext.create(viewGroup.context, viewGroup)))
        view.itemView.setOnClickListener{
            listener.onItemClick(it, view.adapterPosition)
        }
        return view
    }
}

class EventViewHolder(view :View) :RecyclerView.ViewHolder(view) {
    private var dateEvent   : TextView by Delegates.notNull()
    private var homeTeam    : TextView by Delegates.notNull()
    private var scoreHome  : TextView by Delegates.notNull()
    private var scoreAway   : TextView by Delegates.notNull()
    private var awayTeam    : TextView by Delegates.notNull()

    init {
        dateEvent   = view.findViewById(EventUI.date)
        homeTeam    = view.findViewById(EventUI.homeTeamName)
        scoreHome  = view.findViewById(EventUI.scoreHome)
        scoreAway = view.findViewById(EventUI.scoreAway)
        awayTeam    = view.findViewById(EventUI.awayTeamName)
    }

    fun bindItem(item :Event ){
        dateEvent.text  = item.dateEvent.toLocalDate()
        homeTeam.text   = item.homeTeam


        scoreHome.text       = item.homeScore?.toString() ?:""
        scoreAway.text       = item.awayScore?.toString() ?:""
        awayTeam.text   = item.awayTeam

    }
}

class EventUI :AnkoComponent<ViewGroup>{
    companion object {
        const val scoreHome = 3
        const val date = 4
        const val homeTeamName = 5
        const val awayTeamName = 6
        const val scoreAway = 7
    }

    override fun createView(ui: AnkoContext<ViewGroup>): View  = with(ui){
        cardView{
            lparams(width= matchParent, height = wrapContent) {
                margin = dip(2)
            }
        verticalLayout {
            lparams(width = matchParent, height = wrapContent){
                margin = dip(2)
            }
            padding = dip (20)
            backgroundColor = Color.WHITE
            gravity = Gravity.CENTER_HORIZONTAL

            textView{
                id = date
                textSize = 16f
                textColor = Color.BLUE
                gravity = Gravity.CENTER
            }.lparams(width = matchParent, height = wrapContent){
                bottomMargin = dip(10)
            linearLayout{
                gravity = Gravity.CENTER_HORIZONTAL
                lparams(width = matchParent, height = wrapContent)

                textView{
                    id = homeTeamName
                    textSize = 16f
                    gravity = Gravity.CENTER
                }.lparams(width = dip(0), height = wrapContent){
                    weight =0.4f
                }

                textView{
                        id = scoreHome
                        textSize = 16f
                        setTypeface(null, Typeface.BOLD)
                        gravity = Gravity.CENTER
                    }.lparams(width = dip(0), height = wrapContent){
                        weight = 0.1f
                    }

                    textView("VS"){
                        textSize = 12f
                        gravity = Gravity.CENTER
                    }.lparams(width = dip(0), height = wrapContent){
                        weight = 0.1f
                    }

                    textView{
                        id = scoreAway
                        textSize = 16f
                        setTypeface(null, Typeface.BOLD)
                        gravity = Gravity.CENTER
                    }.lparams(width = dip(0), height = wrapContent){
                        weight = 0.1f
                    }





                textView{
                    id = awayTeamName
                    textSize = 16f
                    gravity = Gravity.CENTER
                }.lparams(width = dip(0), height = wrapContent){
                    weight = 0.4f
                }
            }

            }

        }
        }
    }
}
