package personal.dicoding.submission2.home.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseDetailEvent (val events: List<DetailEvent>) : Parcelable