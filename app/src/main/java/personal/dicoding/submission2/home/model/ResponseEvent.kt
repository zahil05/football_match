package personal.dicoding.submission2.home.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseEvent (val events: List<Event>) : Parcelable